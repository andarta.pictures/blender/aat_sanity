from .common.sanity_utils import check_all_gp,check_all_col
from .common.utils import register_classes, unregister_classes
import bpy



    
class AAT_SN_rename_layers(bpy.types.Operator) :
    bl_idname = "sanity.rename_gp_layers"
    bl_label = "rename the gp layers"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):

        log = check_all_gp()
        print(log)

        return {'FINISHED'}
      



classes = [
    AAT_SN_rename_layers,
]

def register() :
    register_classes(classes)

def unregister() :
    unregister_classes(classes)