# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy

class Sanity_Panel(bpy.types.Panel):
    """Creates a Panel in the Object properties window"""
    bl_label = "Sanity"
    bl_idname = "OBJECT_PT_sanitypanel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Andarta"

    def draw(self, context):
    
        row = self.layout.row()
        row.operator("sanity.rename_gp_layers", text="rename gp shapes")



def register() :
    bpy.utils.register_class(Sanity_Panel)

def unregister() :
    bpy.utils.unregister_class(Sanity_Panel)
