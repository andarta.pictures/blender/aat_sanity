import bpy

def ShowMessageBox(message = "", title = "Message Box", icon = 'INFO'):

    def draw(self, context):
        self.layout.label(text=message)

    bpy.context.window_manager.popup_menu(draw, title = title, icon = icon)


def check_all_gp(col=None,log='',gp_prefix = "GP_",gpsh_prefix = 'GPSH_', rename_stroke = True,check_layers = False, show = True):
    if not col:
        col = bpy.data.collections.get('COL-GP')
    log+= 'CHECKIN GP OF COLLECTION : '+col.name+'---\n'


    # Iterate trough each Layer
    for obj in col.all_objects:  
        log+='--' + obj.name + ':'
        if (obj.type == 'GPENCIL'):   
                if obj.name.startswith(gp_prefix):
                    log += 'is OK \n '
                    stroke_name = obj.data.name.replace(gp_prefix, gpsh_prefix, 1) 
                    if stroke_name!= obj.data.name :
                        if rename_stroke: 
                            
                            obj.data.name = stroke_name  
                            log+=' datablock name is off. renaming DONE \n '                   
                        
                        if check_layers:
                            log+= check_all_gplyr(obj,log)
                    
                else:
                        log += 'is not correctly named ! \n'
        else:
            log += ': is not a GP ! \n'
    if show:
        ShowMessageBox(message = log, title = "Sanity check log", icon = 'INFO')        
    return log

def check_all_gplyr(gp,log='',gplyr_prefix = 'GPLYR_' ,rename = False):
    for lyr in gp.data.layers:
        log+= 'layer '+ lyr.info
        if lyr.info.startswith(gplyr_prefix):
            log += 'OK\n'
            pass
        else:
            if rename:
                lyr.info = 'GPLYR_'+lyr.info 
            log += 'is not correctly named !\n'
    return log
     

def check_all_geo(col,log=''):
    return log

def check_all_cam(col,log=''):
    return log

def check_asset_col(col,log=''):
    return log


def check_all_col(log='',gp_col_prefix='COL-GP'):
    log+= '+++ START COLLECTIONS CHECK +++ \n '
    for  col in bpy.data.collections:
        log += col.name + ':'
        found = False
        for k in collection_check_layout:
            if col.name.startswith(k):
                log+= collection_check_layout.get(k)(col) 
                found=True
                log+= "\n DONE \n"
                break
        if not found:
            log += ' is not correctly named !'
    return log
            
          
collection_check_layout = {
     'COL-GP' :  check_all_gp,
     'COL-GEO':  check_all_geo,
     'COL-CAM':  check_all_cam,
     'Asset_emptys_collection': check_asset_col,
}
